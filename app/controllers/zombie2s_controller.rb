class Zombie2sController < ApplicationController
  before_action :set_zombie2, only: [:show, :edit, :update, :destroy]

  # GET /zombie2s
  # GET /zombie2s.json
  def index
    @zombie2s = Zombie2.all
    @rotten_zombies = Zombie2.where(rotten: true)
  end

  # GET /zombie2s/1
  # GET /zombie2s/1.json
  def show
  end

  # GET /zombie2s/new
  def new
    
     @user = current_user
      if @user.role == "subscriber"
      flash[:error] = "No estas autorizado"
      redirect_to zombie2s_path
    else
 
    @zombie2 = Zombie2.new
end
  end

  # GET /zombie2s/1/edit
  def edit
  end

  # POST /zombie2s
  # POST /zombie2s.json
  def create
    @zombie2 = Zombie2.new(zombie2_params)
    @zombie2.user_id = current_user.id 
    respond_to do |format|
      if @zombie2.save
        format.html { redirect_to @zombie2, notice: 'Zombie2 was successfully created.' }
        format.json { render :show, status: :created, location: @zombie2 }
      else
        format.html { render :new }
        format.json { render json: @zombie2.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /zombie2s/1
  # PATCH/PUT /zombie2s/1.json
  def update
    respond_to do |format|
      if @zombie2.update(zombie2_params)
        format.html { redirect_to @zombie2, notice: 'Zombie2 was successfully updated.' }
        format.json { render :show, status: :ok, location: @zombie2 }
      else
        format.html { render :edit }
        format.json { render json: @zombie2.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /zombie2s/1
  # DELETE /zombie2s/1.json
  def destroy
    @zombie2.destroy
    respond_to do |format|
      format.html { redirect_to zombie2s_url, notice: 'Zombie2 was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_zombie2
      @zombie2 = Zombie2.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def zombie2_params
      params.require(:zombie2).permit(:Avatar,:name, :bio, :age, :email, :rotten, :user_id)
    end
end
