class Brain < ApplicationRecord
	belongs_to:zombie2

	validates :flavor, presence: true
	validates :iq, numericality: {only_integer: true, message:"Solo numeros enteros"}
end
