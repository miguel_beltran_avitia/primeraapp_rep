class Zombie2 < ApplicationRecord
	has_many :brains
	belongs_to :user
	mount_uploader :Avatar, AvatarUploader
	validates :name, presence: true
 	validates :bio, length: { maximum: 100 }
	validates :age, numericality: {only_integer: true, message:"Solo numeros enteros"} 
	validates :email, format: {	with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i}
end
