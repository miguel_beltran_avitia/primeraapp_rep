json.extract! zombie2, :id, :name, :bio, :age, :created_at, :updated_at
json.url zombie2_url(zombie2, format: :json)
