class AddZombie2ToBrain < ActiveRecord::Migration[5.0]
  def change
    add_reference :brains, :zombie2, foreign_key: true
  end
end
