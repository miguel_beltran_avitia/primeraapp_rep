require 'test_helper'

class Zombie2sControllerTest < ActionDispatch::IntegrationTest
  setup do
    @zombie2 = zombie2s(:one)
  end

  test "should get index" do
    get zombie2s_url
    assert_response :success
  end

  test "should get new" do
    get new_zombie2_url
    assert_response :success
  end

  test "should create zombie2" do
    assert_difference('Zombie2.count') do
      post zombie2s_url, params: { zombie2: { age: @zombie2.age, bio: @zombie2.bio, name: @zombie2.name } }
    end

    assert_redirected_to zombie2_url(Zombie2.last)
  end

  test "should show zombie2" do
    get zombie2_url(@zombie2)
    assert_response :success
  end

  test "should get edit" do
    get edit_zombie2_url(@zombie2)
    assert_response :success
  end

  test "should update zombie2" do
    patch zombie2_url(@zombie2), params: { zombie2: { age: @zombie2.age, bio: @zombie2.bio, name: @zombie2.name } }
    assert_redirected_to zombie2_url(@zombie2)
  end

  test "should destroy zombie2" do
    assert_difference('Zombie2.count', -1) do
      delete zombie2_url(@zombie2)
    end

    assert_redirected_to zombie2s_url
  end
end
